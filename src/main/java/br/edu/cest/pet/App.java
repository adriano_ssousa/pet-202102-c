package br.edu.cest.pet;

import br.edu.cest.pet.divisao.Clinica;
import br.edu.cest.pet.divisao.Setor;
import br.edu.cest.pet.entity.Cliente;
import br.edu.cest.pet.entity.Empresa;
import br.edu.cest.pet.entity.arca.Animal;
import br.edu.cest.pet.entity.staff.Atendente;
import br.edu.cest.pet.entity.staff.Veterinario;
import br.edu.cest.pet.vo.EnTurno;

/**
 * Hello world!
 *
 */
public class App {
	public static void main(String[] args) {
		System.out.println("Bem vindo!");
		
		Cliente c = new  Cliente();
		c.setCpf("1111111");
		c.setNome("Josue");
		c.setSexo('M');
		

		Veterinario f = new  Veterinario("Josue", EnTurno.MAT, "123");
		f.setCpf("1111111");
		f.setNome("Josue");
		f.setSexo('M');
		System.out.println(f.getCategoria());
		
		Atendente atend = new  Atendente("Antonia", EnTurno.VESP, "124");
		atend.setCpf("1111111");
		atend.setSexo('M');
		
		Empresa acme = new Empresa("ACME SA", "01010101010101");
		//acme.contrata(atend);
		//acme.contrata(f);
		
		acme.listarFuncionarios();
		
		Animal a = new Animal("Tigresa Gigante Voadora Surreal", "tigre");
		System.out.println(a);
		
		//TODO 
		acme.listaFuncionarios.add(f);
		
		acme.listarFuncionarios();

	}
}
